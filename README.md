# reparcs nigulp #
[![build status](https://travis-ci.org/weirdpercent/reparcs-nigulp.png?branch=master)](https://travis-ci.org/weirdpercent/reparcs-nigulp)[![dependencies](https://gemnasium.com/weirdpercent/reparcs-nigulp.png)](https://gemnasium.com/weirdpercent/reparcs-nigulp)[![coverage](https://coveralls.io/repos/weirdpercent/reparcs-nigulp/badge.png)](https://coveralls.io/r/weirdpercent/reparcs-nigulp)[![Code Climate](https://codeclimate.com/github/weirdpercent/reparcs-nigulp.png)](https://codeclimate.com/github/weirdpercent/reparcs-nigulp)[ ![Codeship Status for weirdpercent/reparcs-nigulp](https://www.codeship.io/projects/6f41f470-03ed-0131-a876-26a681687fd7/status?branch=master)](https://www.codeship.io/projects/7181)

soundsplus is an audio freeware database like [KVR Audio](http://www.kvraudio.com/). This is the KVR scraper module.

* First stage works, it saves the link for each released, free product on KVR to plinks.txt.
* Second stage works, scraping data for each product into a JSON file.
* CouchDB interface via couchrest works

## ToDo: ##

* Write tests

## Gratitude ##

Many thanks to the authors of the gems used in this module!
